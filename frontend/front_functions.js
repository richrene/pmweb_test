
//FUNCTIONS TO SUPPORT FRONTEND

var remoteServer = "http://localhost:3000/"

//Load JSON File from Local Machine
var jsonDataRaw
function loadLocalFunc() {
  var input, file, fr
  
  input = document.getElementById('fileinput')
  file = input.files[0]
  fr = new FileReader()
  fr.onload = (function (e) {
    lines = e.target.result
    jsonDataRaw = JSON.parse(lines)
    loadChart(jsonDataRaw)
  })
  fr.readAsText(file)
}

//Load JSON File from Remote Server (using GET)
function loadServerFunc() {
  $.getJSON(remoteServer, function(jsonData) { loadChart(jsonData) })
  .error(function() { alert("Internal Server Error!") })
}

//Upload JSON File to Remote Server (using POST)
function uploadServerFunc() {
  if (jsonDataRaw) {
    $.post(remoteServer, jsonDataRaw)
    .done(function() { alert( "File stored with success!" ) })
    .error(function() { alert("Internal Server Error!") })
  } else {
    alert("Load from local file first!")
  }
}

//Load JSON File to Highcharts Chart
function loadChart(jsonData) {
  var title = {
    text: jsonData.metrics.revenue_by_medium.title.toUpperCase(),
    align: 'left'
  }

  var subtitle = {
    text: jsonData.metrics.revenue_by_medium.description,
    align: 'left',
    y: 30
  }

  var xAxis = {
    type: 'datetime',
    dateTimeLabelFormats: {day: '%e'} //FIX: only days in the xAxis
  }

  var yAxis = {
    title: {
      text: '' //You may add a yAxis Title here (if necessary)
    }
  }

  var tooltip = {
    valueDecimals: 2,
    valuePrefix: '$ ', //FIX: Currency symbol addition
    valueSuffix: ' BRL', //FIX: Currency symbol addition
    xDateFormat: '%d/%m/%Y' //FIX: Date in brazilian format
  }

  var legend = {
    layout: 'horizontal',
    verticalAlign: 'top',
    borderWidth: 0
  }

  var chart = {
    type: 'spline' //Smooths the line corners
  }

  var plotOptions = {
    series: {
      marker: {
        enabled: false //FIX: disables the point marker
      }
    }
  }

  //FIX: date in the xAxis
  var startDate = jsonData.start_date.split('-')
  var pointStart = Date.UTC(startDate[0], startDate[1] - 1, startDate[2])
  var pointInterval = 24 * 3600 * 1000 //one day

  //Creates an object structure for the "series" parameter
  var series = {
    "affiliates":  {name: "affiliates",  data: [], pointStart: pointStart, pointInterval: pointInterval},
    "email":       {name: "email",       data: [], pointStart: pointStart, pointInterval: pointInterval},
    "organic":     {name: "organic",     data: [], pointStart: pointStart, pointInterval: pointInterval},
    "other":       {name: "other",       data: [], pointStart: pointStart, pointInterval: pointInterval},
    "paid search": {name: "paid search", data: [], pointStart: pointStart, pointInterval: pointInterval},
    "referral":    {name: "referral",    data: [], pointStart: pointStart, pointInterval: pointInterval},
    "retargeting": {name: "retargeting", data: [], pointStart: pointStart, pointInterval: pointInterval},
    "social":      {name: "social",      data: [], pointStart: pointStart, pointInterval: pointInterval},
    "social paid": {name: "social paid", data: [], pointStart: pointStart, pointInterval: pointInterval}
  }

  //FIX: adjust the data (coming in the JSON file) to fit the "series" parameter (it must be an array with all values)
  //FIX: change "," to "." before the data conversion (string to number)
  jsonData.metrics.revenue_by_medium.data.series.forEach((item) => {
    series["affiliates"].data.push(Number(item.data["affiliates"].replace(",",".")))
    series["email"].data.push(Number(item.data["email"].replace(",",".")))
    series["organic"].data.push(Number(item.data["organic"].replace(",",".")))
    series["other"].data.push(Number(item.data["other"].replace(",",".")))
    series["paid search"].data.push(Number(item.data["paid search"].replace(",",".")))
    series["referral"].data.push(Number(item.data["referral"].replace(",",".")))
    series["retargeting"].data.push(Number(item.data["retargeting"].replace(",",".")))
    series["social"].data.push(Number(item.data["social"].replace(",",".")))
    series["social paid"].data.push(Number(item.data["social paid"].replace(",",".")))
  })

  //Converts the object to an array with the same elements
  series = Object.keys(series).map(key => series[key])

  //Mounts the JSON in the format accepted by the highcharts
  var json = {}
  json.title = title
  json.subtitle = subtitle
  json.xAxis = xAxis
  json.yAxis = yAxis
  json.tooltip = tooltip
  json.legend = legend
  json.series = series
  json.chart = chart
  json.plotOptions = plotOptions

  //Loads the JSON to highcharts
  $('#container').highcharts(json);

}
