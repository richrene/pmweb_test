
//---------------------------------------------------------------
// INCLUDES AND VARIABLES

var express = require('express')
var bodyParser = require('body-parser')
var fs = require('fs')

var port = 3000
var app = express()
var router = express.Router()

//---------------------------------------------------------------
// ROUTES

router.get('/', (request, response) => {
  
  fs.readFile('get_metrics.json', (err, jsonData) => {
  	response.setHeader('Access-Control-Allow-Origin', '*') //Added to prevent the browser from blocking the responses

    if (err) {
      response.status(500).json({ error: err })
      console.log(err)
    } else {
      response.status(200).json(JSON.parse(jsonData))
    }

  })

})

router.post('/', (request, response) => {  
  
  fs.writeFile('get_metrics.json', JSON.stringify(request.body), (err) => {
    response.setHeader('Access-Control-Allow-Origin', '*') //Added to prevent the browser from blocking the responses

    if (err) {
      response.status(500).json({ error: err })
      console.log(err)
    } else {
      response.status(200).send()
    }

  })
  
})

//---------------------------------------------------------------
// SERVER

// MIDDLEWARES
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use('/', router)

// LISTEN
app.listen(port, () => { 
  console.log(`Server is up on port ${port}`) 
})

//---------------------------------------------------------------
