
# HIGHCHARTS TEST

## Technologies used in the Project
Editors
- Sublime
- Notepad++

Versioning and Repository
- Git
- Bitbucket

Languages, Platforms and Frameworks
- Frontend: HTML, CSS, Javascript, jQuery
- Backend: Node.js, Javascript, npm

Test and Debug
- Chrome Developer Tools
- Postman

## Assumptions
Git is installed in your machine.
Node.js is installed in your machine.

## Repository
To download content: 
git clone https://richrene@bitbucket.org/richrene/pmweb_test.git

## Starting Frontend
- Go to the frontend folder you downloaded (pmweb_test/frontend).
- Open the file "frontend.html" in you local browser.

## Staring Backend
- Go to the backend folder you downloaded (pmweb_test/backend).
- Run the command: npm install
- Run the command: node backend.js

## Suggestions to the Backend Team (regarding the JSON file)
- The data related to each serie should fit the "series data" parameter. It must be an array with all values of that single serie.
- The data present in the serie should be a "number", with "." instead of "," for decimal values.
- Add to the serie the "pointStart" (converted from the "start_date" field) and the "pointInterval" (a number representing the interval that will be used in the xAxis).
- Besides, any other transformation that could make the JSON content look more like the parameters used by Highcharts could be discussed.
